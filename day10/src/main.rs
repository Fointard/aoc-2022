const INPUT: &str = include_str!("input.txt");
const CRT_WIDTH: usize = 40;
const CRT_SIZE: usize = CRT_WIDTH * 6;

fn main() {
	solve_part_1();
	solve_part_2();
}

fn solve_part_1() -> isize {
	let (mut cycle, mut x) = (1usize, 1isize);

	let mut interesting = (20usize..=220usize).step_by(40).peekable();
	let mut strenghts = Vec::new();

	INPUT.lines().for_each(|l| {
		let old_x = x;

		match l {
			"noop" => cycle += 1,
			_ => {
				cycle += 2;
				x += l
					.split_ascii_whitespace()
					.nth(1)
					.unwrap()
					.parse::<isize>()
					.unwrap();
			}
		};

		if let Some(n) = interesting.peek() {
			if &cycle >= n {
				strenghts.push(*n as isize * if &cycle > n { old_x } else { x });
				interesting.next();
			}
		}
	});

	let answer = strenghts.iter().sum();
	println!("part 1 answer: {}", answer);
	answer
}

fn solve_part_2() -> [char; CRT_SIZE] {
	let (mut cycle, mut x) = (1usize, 1isize);

	let mut answer = [' '; CRT_SIZE];

	INPUT.lines().for_each(|l| {
		let old_cycle = cycle;
		let sprite_range = (x - 1)..=(x + 1);

		match l {
			"noop" => cycle += 1,
			_ => {
				cycle += 2;
				x += l
					.split_ascii_whitespace()
					.nth(1)
					.unwrap()
					.parse::<isize>()
					.unwrap();
			}
		};

		(old_cycle..cycle).for_each(|cycle| {
			let drawn_pixel = ((cycle - 1) % CRT_WIDTH) as isize;
			if (sprite_range).contains(&drawn_pixel) {
				answer[cycle - 1] = '#';
			}
		});
	});

	println!("part 2 answer:");
	answer.chunks(CRT_WIDTH).for_each(|c| println!("{:?}", c));
	answer
}

#[test]
fn part_1() {
	assert_eq!(solve_part_1(), 14620isize);
}

#[test]
fn part_2() {
	assert_eq!(
		solve_part_2(),
		//BJFRHRFU
		[
			'#', '#', '#', ' ', ' ', ' ', ' ', '#', '#', ' ', '#', '#', '#', '#', ' ', '#', '#', '#', ' ', ' ', '#', ' ', ' ', '#', ' ', '#', '#', '#', ' ', ' ', '#', '#', '#', '#', ' ', '#', ' ', ' ', '#', ' ',
			'#', ' ', ' ', '#', ' ', ' ', ' ', ' ', '#', ' ', '#', ' ', ' ', ' ', ' ', '#', ' ', ' ', '#', ' ', '#', ' ', ' ', '#', ' ', '#', ' ', ' ', '#', ' ', '#', ' ', ' ', ' ', ' ', '#', ' ', ' ', '#', ' ',
			'#', '#', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', '#', '#', '#', ' ', ' ', '#', ' ', ' ', '#', ' ', '#', '#', '#', '#', ' ', '#', ' ', ' ', '#', ' ', '#', '#', '#', ' ', ' ', '#', ' ', ' ', '#', ' ',
			'#', ' ', ' ', '#', ' ', ' ', ' ', ' ', '#', ' ', '#', ' ', ' ', ' ', ' ', '#', '#', '#', ' ', ' ', '#', ' ', ' ', '#', ' ', '#', '#', '#', ' ', ' ', '#', ' ', ' ', ' ', ' ', '#', ' ', ' ', '#', ' ',
			'#', ' ', ' ', '#', ' ', '#', ' ', ' ', '#', ' ', '#', ' ', ' ', ' ', ' ', '#', ' ', '#', ' ', ' ', '#', ' ', ' ', '#', ' ', '#', ' ', '#', ' ', ' ', '#', ' ', ' ', ' ', ' ', '#', ' ', ' ', '#', ' ',
			'#', '#', '#', ' ', ' ', ' ', '#', '#', ' ', ' ', '#', ' ', ' ', ' ', ' ', '#', ' ', ' ', '#', ' ', '#', ' ', ' ', '#', ' ', '#', ' ', ' ', '#', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', '#', ' ', ' '
		]
	);
}
