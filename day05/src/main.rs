const INPUT: &str = include_str!("input.txt");

fn main() {
	solve_part_1();
	solve_part_2();
}

fn solve<F: FnMut(&mut Vec<Vec<char>>, (usize, usize, usize))>(mut crane: F) -> String {
	let mut lines = INPUT.lines();

	// Get a vector of moves
	let mut moves = lines
		.by_ref()
		.rev()
		.take_while(|l| !l.is_empty())
		.map(|l| {
			let mut split = l
				.split_ascii_whitespace()
				.skip(1)
				.step_by(2)
				.map(|n| n.parse::<usize>().unwrap());
			(
				split.next().unwrap(),
				split.next().unwrap() - 1,
				split.next().unwrap() - 1,
			)
		})
		.collect::<Vec<_>>();

	let stacks_nb = lines
		.by_ref()
		.rev()
		.next()
		.unwrap()
		.rsplit(|c: char| c.is_ascii_whitespace())
		.nth(1)
		.unwrap()
		.parse::<usize>()
		.unwrap();

	let mut stacks = vec![Vec::<char>::new(); stacks_nb];

	// Populate the stacks
	lines.rev().for_each(|l| {
		l.char_indices()
			.filter(|(_, c)| c.is_ascii_alphabetic())
			.for_each(|(i, c)| stacks[i / 4].push(c));
	});

	// Make the crane operator work
	while let Some(christmas_move) = moves.pop() {
		crane(&mut stacks, christmas_move);
	}

	// Get a "top crates" String
	stacks
		.into_iter()
		.fold(String::new(), |mut top_line, mut crates| {
			top_line.push(crates.pop().unwrap());
			top_line
		})
}

fn solve_part_1() -> String {
	let answer = solve(|stacks, (m, f, t)| {
		for _ in 0..m {
			let christmas_crate = stacks[f].pop().unwrap();
			stacks[t].push(christmas_crate);
		}
	});

	println!("part 1 answer: {}", answer);
	answer
}

fn solve_part_2() -> String {
	let answer = solve(|stacks, (m, f, t)| {
		let range = stacks[f].len() - m..;
		let tmp = stacks[f].drain(range).collect::<Vec<_>>();
		stacks[t].extend(tmp);
	});

	println!("part 2 answer: {}", answer);
	answer
}

#[test]
fn part_1() {
	assert_eq!(solve_part_1(), "CVCWCRTVQ");
}

#[test]
fn part_2() {
	assert_eq!(solve_part_2(), "CNSCZWLVT");
}
