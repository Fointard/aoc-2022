const INPUT: &str = include_str!("input.txt");
const SIZE: usize = 99;

fn main() {
	solve_part_1();
	solve_part_2();
}

fn get_trees() -> impl Iterator<Item = (usize, usize)> {
	INPUT
		.chars()
		.filter_map(|c| {
			if !c.is_ascii_whitespace() {
				Some(c.to_digit(10).unwrap() as usize)
			} else {
				None
			}
		})
		.enumerate()
}

fn get_vecs(idx: &usize) -> [(Vec<usize>, Vec<usize>); 4] {
	let (x, y) = idx_to_coord(idx);
	let (h_prev, h_next) = get_trees()
		.filter(|(i, _)| i / SIZE == x && i != idx)
		.partition::<(Vec<_>, Vec<_>), _>(|(i, _)| i < idx);

	let (v_prev, v_next) = get_trees()
		.filter(|(i, _)| i % SIZE == y && i != idx)
		.partition::<(Vec<_>, Vec<_>), _>(|(i, _)| i < idx);

	[h_prev, h_next, v_prev, v_next]
}

fn idx_to_coord(idx: &usize) -> (usize, usize) {
	(idx / SIZE, idx % SIZE)
}

fn is_visible(idx: &usize, height: &usize) -> bool {
	let vecs = get_vecs(idx);
	vecs.iter().any(|v| v.1.iter().all(|h| h < height))
}

fn scenic_score(idx: &usize, height: &usize) -> usize {
	let vecs = get_vecs(idx);

	let (x, y) = idx_to_coord(idx);
	if x == 0 || y == 0 || x == SIZE - 1 || y == SIZE - 1 {
		return 0;
	}

	vecs.iter()
		.enumerate()
		.map(|(i, (_, vec))| {
			let iter: Vec<&usize> = if i % 2 == 0 {
				vec.iter().rev().collect()
			} else {
				vec.iter().collect()
			};

			let count = iter.iter().take_while(|&&h| h < height).count();
			count + usize::from(count < iter.len())
		})
		.product()
}

fn solve<F: FnMut(usize, (usize, usize)) -> usize>(closure: F) -> usize {
	get_trees().fold(usize::MIN, closure)
}

fn solve_part_1() -> usize {
	let answer = solve(|acc, (ref i, ref h)| acc + usize::from(is_visible(i, h)));
	println!("part 1 answer: {}", answer);
	answer
}

fn solve_part_2() -> usize {
	let answer = solve(|acc, (ref i, ref h)| std::cmp::max(acc, scenic_score(i, h)));
	println!("part 2 answer: {}", answer);
	answer
}

#[test]
fn part_1() {
	assert_eq!(solve_part_1(), 1705usize);
}

#[test]
fn part_2() {
	assert_eq!(solve_part_2(), 371200usize);
}
