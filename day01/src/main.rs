use std::collections::BinaryHeap;

const INPUT: &str = include_str!("input.txt");

fn main() {
	let mut heap = sort_calories();

	solve_part_1(&mut heap.clone());
	solve_part_2(&mut heap);
}

fn sort_calories() -> BinaryHeap<u32> {
	let mut sorted_calories = BinaryHeap::<u32>::new();

	INPUT.lines().fold(0, |acc, itm| {
		if let Ok(n) = itm.parse::<u32>() {
			acc + n
		} else {
			sorted_calories.push(acc);
			0
		}
	});

	sorted_calories
}

fn solve_part_1(heap: &mut BinaryHeap<u32>) -> u32 {
	let answer = heap.pop().unwrap();
	println!("part 1 answer: {}", answer);
	answer
}

fn solve_part_2(heap: &mut BinaryHeap<u32>) -> u32 {
	let mut answer = 0;
	(0..3).for_each(|_| answer += heap.pop().unwrap());
	println!("part 2 answer: {}", answer);
	answer
}

#[test]
fn part_1() {
	let mut heap = sort_calories();
	assert_eq!(solve_part_1(&mut heap), 66186);
}

#[test]
fn part_2() {
	let mut heap = sort_calories();
	assert_eq!(solve_part_2(&mut heap), 196804u32);
}
