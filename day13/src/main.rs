mod list;

use std::collections::BTreeSet;

use list::List;

const INPUT: &str = include_str!("input.txt");

fn main() {
	solve_part_1();
	solve_part_2();
}

fn solve_part_1() -> usize {
	let answer = INPUT
		.lines()
		.step_by(3)
		.zip(INPUT.lines().skip(1).step_by(3))
		.enumerate()
		.map(|(i, (l, r))| {
			(i + 1) * usize::from(List::try_from(l).unwrap() < List::try_from(r).unwrap())
		})
		.sum();

	println!("part 1 answer: {}", answer);
	answer
}

fn solve_part_2() -> usize {
	let div_packets = [
		List::try_from("[[2]]").unwrap(),
		List::try_from("[[6]]").unwrap(),
	];

	let answer = INPUT
		.lines()
		.filter_map(|l| (!l.is_empty()).then(|| List::try_from(l).unwrap()))
		.chain(div_packets.clone())
		.collect::<BTreeSet<_>>()
		.iter()
		.enumerate()
		.filter_map(|(idx, l)| div_packets.contains(l).then_some(idx + 1))
		.product::<usize>();

	println!("part 2 answer: {}", answer);
	answer
}

#[test]
fn part_1() {
	assert_eq!(solve_part_1(), 4894usize);
}

#[test]
fn part_2() {
	assert_eq!(solve_part_2(), 24180usize);
}
