use super::List;

pub struct ListIntoIter<'l> {
	pub(crate) list: &'l List<'l>,
	pub(crate) index: usize,
}

impl<'l> Iterator for ListIntoIter<'l> {
	type Item = &'l str;

	fn next(&mut self) -> Option<Self::Item> {
		if self.index == self.list.len() {
			return None;
		}

		let mut lvl = 0;
		let old_index = self.index;

		for c in self.list[self.index..].chars() {
			self.index += 1;
			match c {
				'[' => lvl += 1,
				']' => lvl -= 1,
				',' if lvl == 0 => {
					return Some(&self.list[old_index..self.index - 1]);
				}
				_ => (),
			}
		}

		Some(&self.list[old_index..self.index])
	}
}
