mod list_into_iter;

use std::{cmp::Ordering, ops::Deref};

use list_into_iter::ListIntoIter;

#[derive(Clone, Debug, Eq, Ord, PartialEq)]
pub struct List<'l>(&'l str);

impl<'l> Deref for List<'l> {
	type Target = &'l str;

	fn deref(&self) -> &Self::Target {
		&self.0
	}
}

impl<'l> IntoIterator for &'l List<'l> {
	type Item = &'l str;
	type IntoIter = ListIntoIter<'l>;

	fn into_iter(self) -> Self::IntoIter {
		ListIntoIter {
			list: self,
			index: usize::MIN,
		}
	}
}

impl<'l> PartialOrd for List<'l> {
	fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
		let (mut l, mut r) = (self.into_iter(), other.into_iter());

		loop {
			break Some(match (l.next(), r.next()) {
				(None, None) => Ordering::Equal,
				(None, Some(_)) => Ordering::Less,
				(Some(_), None) => Ordering::Greater,
				(Some(l), Some(r)) => match (List::try_from(l), List::try_from(r)) {
					(Err(l), Err(r)) => {
						match l.parse::<u8>().unwrap().cmp(&r.parse::<u8>().unwrap()) {
							Ordering::Equal => continue,
							cmp => cmp,
						}
					}
					(Ok(l), Ok(ref r)) | (Ok(l), Err(ref r)) | (Err(l), Ok(ref r)) => {
						match l.partial_cmp(r).unwrap() {
							Ordering::Equal => continue,
							cmp => cmp,
						}
					}
				},
			});
		}
	}
}

impl<'l> TryFrom<&'l str> for List<'l> {
	type Error = List<'l>;

	fn try_from(s: &'l str) -> Result<Self, Self> {
		if s.starts_with('[') {
			Ok(List(&s[1..(s.len() - 1)]))
		} else {
			Err(List(s))
		}
	}
}
