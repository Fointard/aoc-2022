const INPUT: &str = include_str!("input.txt");

fn main() {
	solve_part_1();
	solve_part_2();
}

fn solve_part_1() -> u32 {
	let answer = INPUT.lines().fold(0, |acc, itm| {
		acc + match itm {
			"A X" => 4,
			"A Y" => 8,
			"A Z" => 3,
			"B X" => 1,
			"B Y" => 5,
			"B Z" => 9,
			"C X" => 7,
			"C Y" => 2,
			"C Z" => 6,
			_ => panic!("unknown string"),
		}
	});

	println!("part 1 answer: {}", answer);
	answer
}

fn solve_part_2() -> u32 {
	let answer = INPUT.lines().fold(0, |acc, itm| {
		acc + match itm {
			"A X" => 3,
			"A Y" => 4,
			"A Z" => 8,
			"B X" => 1,
			"B Y" => 5,
			"B Z" => 9,
			"C X" => 2,
			"C Y" => 6,
			"C Z" => 7,
			_ => panic!("unknown string"),
		}
	});

	println!("part 2 answer: {}", answer);
	answer
}

#[test]
fn part_1() {
	assert_eq!(solve_part_1(), 17189u32);
}

#[test]
fn part_2() {
	assert_eq!(solve_part_2(), 13490u32);
}
