use pathfinding::{directed::dijkstra::dijkstra, grid::Grid};

const INPUT: &str = include_str!("input.txt");

fn main() {
	solve_part_1();
	solve_part_2();
}

fn coords_to_idx(coords: (usize, usize), w: &usize) -> usize {
	w * coords.1 + coords.0
}

fn idx_to_coords(idx: usize, w: usize) -> (usize, usize) {
	(idx % w, idx / w)
}

fn solve<const PART: usize>() -> usize {
	let w = INPUT.find('\n').unwrap();
	let h = (INPUT.len() + 1) / (w + 1);

	let mut grid = Grid::new(w, h);
	grid.fill();

	let map = INPUT
		.chars()
		.filter(|c| !c.is_ascii_whitespace())
		.collect::<String>();

	let (start, end) = (map.find('S').unwrap(), map.find('E').unwrap());

	let heights = map
		.chars()
		.map(|c| match c {
			'S' => 1,
			'E' => 26,
			c => u8::try_from(c).unwrap() - 96,
		})
		.collect::<Vec<u8>>();

	dijkstra(
		&idx_to_coords(end, w),
		|&n| {
			grid.neighbours(n)
				.iter()
				.filter(|&&neighbour| {
					heights[coords_to_idx(neighbour, &w)] >= heights[coords_to_idx(n, &w)] - 1
				})
				.map(|&n| (n, 1))
				.collect::<Vec<((_, _), usize)>>()
		},
		|&n| match PART {
			1 => n == idx_to_coords(start, w),
			2 => heights[coords_to_idx(n, &w)] == 1,
			_ => panic!("Unknown part"),
		},
	)
	.unwrap()
	.1
}

fn solve_part_1() -> usize {
	let answer = solve::<1>();
	println!("part 1 answer: {}", answer);
	answer
}

fn solve_part_2() -> usize {
	let answer = solve::<2>();
	println!("part 2 answer: {}", answer);
	answer
}

#[test]
fn part_1() {
	assert_eq!(solve_part_1(), 472usize);
}

#[test]
fn part_2() {
	assert_eq!(solve_part_2(), 465usize);
}
