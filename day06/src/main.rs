use std::collections::HashSet;

const INPUT: &str = include_str!("input.txt");

fn main() {
	solve_part_1();
	solve_part_2();
}

fn solve<const SIZE: usize>() -> usize {
	let mut answer = SIZE;
	for window in INPUT.chars().collect::<Vec<_>>().windows(SIZE) {
		if window.iter().collect::<HashSet<_>>().len() == SIZE {
			break;
		}
		answer += 1;
	}
	answer
}

fn solve_part_1() -> usize {
	let answer = solve::<4>();
	println!("part 1 answer: {}", answer);
	answer
}

fn solve_part_2() -> usize {
	let answer = solve::<14>();
	println!("part 2 answer: {}", answer);
	answer
}

#[test]
fn part_1() {
	assert_eq!(solve_part_1(), 1134usize);
}

#[test]
fn part_2() {
	assert_eq!(solve_part_2(), 2263usize);
}
