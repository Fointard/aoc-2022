use std::collections::HashSet;

const INPUT: &str = include_str!("input.txt");

fn main() {
	solve_part_1();
	solve_part_2();
}

fn get_common_item<'a, S: IntoIterator<Item = &'a str>>(strs: S) -> char {
	let mut strs = strs.into_iter();
	let first = strs.next().unwrap().chars().collect::<HashSet<_>>();

	strs.fold(first, |acc, itm| {
		acc.intersection(&itm.chars().collect::<HashSet<_>>())
			.copied()
			.collect::<HashSet<_>>()
	})
	.drain()
	.next()
	.unwrap()
}

fn get_priority(c: char) -> u32 {
	c as u32 - 38 - if c.is_ascii_lowercase() { 58 } else { 0 }
}

fn solve_part_1() -> u32 {
	let answer = INPUT.lines().fold(0u32, |acc, itm| {
		let compartments = itm.split_at(itm.len() / 2);
		let compartments = [compartments.0, compartments.1];

		acc + get_priority(get_common_item(compartments))
	});

	println!("part 1 answer: {}", answer);
	answer
}

fn solve_part_2() -> u32 {
	let mut answer = 0;
	let mut lines = INPUT.lines();

	while let (Some(a), Some(b), Some(c)) = (lines.next(), lines.next(), lines.next()) {
		answer += get_priority(get_common_item([a, b, c]));
	}

	println!("part 2 answer: {}", answer);
	answer
}

#[test]
fn part_1() {
	assert_eq!(solve_part_1(), 7878u32);
}

#[test]
fn part_2() {
	assert_eq!(solve_part_2(), 2760u32);
}
