use std::{cell::RefCell, collections::BTreeSet, ops::Deref, rc::Rc};

#[derive(Debug)]
pub enum FileOrFolder<'n> {
	File(usize),
	Folder(&'n str, Option<Rc<Self>>, RefCell<Vec<Rc<Self>>>),
}

impl<'n> FileOrFolder<'n> {
	pub const THRESHOLD: usize = 100_000;

	pub const DISK_SPACE: usize = 70_000_000;
	pub const NEEDED_SPACE: usize = 30_000_000;

	pub fn cd(&self, target: &str) -> Rc<Self> {
		if let Self::Folder(_, _, content) = self {
			return content
				.borrow()
				.iter()
				.find(|fof| matches!(fof.as_ref(), Self::Folder(name, _, _) if *name == target))
				.unwrap()
				.clone();
		}
		panic!("This function can't be used on files");
	}

	fn create(&self, file_or_folder: Self) {
		if let Self::Folder(_, _, content) = self {
			content.borrow_mut().push(Rc::new(file_or_folder));
		} else {
			panic!("This function can't be used on files");
		}
	}

	pub fn create_subfolder(self: Rc<Self>, subfolder: &'n str) {
		self.create(Self::Folder(
			subfolder,
			Some(self.clone()),
			RefCell::new(Vec::new()),
		));
	}

	pub fn create_file(&self, size: usize) {
		self.create(Self::File(size));
	}

	pub fn get_small_folders_total_size(&self) -> usize {
		self.get_subfolders()
			.iter()
			.filter_map(|f| {
				if let f @ Self::Folder(..) = f.as_ref() {
					let size = f.size();
					return if size <= Self::THRESHOLD {
						Some(size)
					} else {
						None
					};
				}
				panic!("This function can't be used on files");
			})
			.sum()
	}

	pub fn get_smallest_eligible_file_size(&self) -> usize {
		let min_size = Self::NEEDED_SPACE - (Self::DISK_SPACE - self.size());
		*self
			.get_subfolders()
			.iter()
			.map(|sf| sf.size())
			.collect::<BTreeSet<_>>()
			.range(min_size..)
			.next()
			.unwrap()
	}

	pub fn get_subfolders(&self) -> Vec<Rc<Self>> {
		if let Self::Folder(_, _, content) = self {
			let mut direct_subfolders = content
				.borrow()
				.iter()
				.filter(|sf| matches!(sf.as_ref(), Self::Folder(..)))
				.cloned()
				.collect::<Vec<_>>();

			let mut subfolders = Vec::new();

			direct_subfolders.iter().for_each(|sf| {
				subfolders.append(&mut sf.get_subfolders());
			});

			subfolders.append(&mut direct_subfolders);

			return subfolders;
		}
		panic!("This function can't be used on files");
	}

	pub fn new(cmds: &'n str) -> Rc<FileOrFolder<'n>> {
		let root_folder = Rc::new(FileOrFolder::Folder("/", None, RefCell::new(Vec::new())));
		let mut current_folder = root_folder.clone();
		cmds.lines().for_each(|l| match l {
			"$ cd /" | "$ ls" => (),
			l if l.starts_with("$ c") => {
				let path = &l[5..];
				if matches!(path, "..") {
					current_folder = current_folder.one_lvl_up();
				} else {
					current_folder = current_folder.cd(path);
				}
			}
			l if l.starts_with('d') => {
				current_folder.clone().create_subfolder(&l[4..]);
			}
			l if l.starts_with(|c: char| c.is_ascii_digit()) => {
				let size = l.split_ascii_whitespace().next().unwrap().parse().unwrap();
				current_folder.create_file(size);
			}
			_ => panic!("Unhandled case"),
		});
		root_folder
	}

	pub fn one_lvl_up(&self) -> Rc<Self> {
		if let Self::Folder(_, Some(parent), _) = self {
			return parent.clone();
		}
		panic!("This function can't be used on files");
	}

	pub fn size(&self) -> usize {
		if let Self::Folder(_, _, content) = self {
			return content.borrow().iter().fold(0, |size, item| {
				size + match item.deref() {
					Self::File(filesize) => *filesize,
					f @ Self::Folder(..) => f.size(),
				}
			});
		}
		panic!("This function can't be used on files");
	}
}
