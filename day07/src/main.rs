mod file_or_folder;

use file_or_folder::FileOrFolder;

const INPUT: &str = include_str!("input.txt");

fn main() {
	solve_part_1();
	solve_part_2();
}

fn solve_part_1() -> usize {
	let root_folder = FileOrFolder::new(INPUT);
	let answer = root_folder.get_small_folders_total_size();
	println!("part 1 answer: {}", answer);
	answer
}

fn solve_part_2() -> usize {
	let root_folder = FileOrFolder::new(INPUT);
	let answer = root_folder.get_smallest_eligible_file_size();
	println!("part 2 answer: {}", answer);
	answer
}

#[test]
fn part_1() {
	assert_eq!(solve_part_1(), 1513699usize);
}

#[test]
fn part_2() {
	assert_eq!(solve_part_2(), 7991939usize);
}
