use std::cell::RefCell;

pub struct Monkey {
	pub items: RefCell<Vec<usize>>,
	pub operation: Box<dyn Fn(usize) -> usize>,
	pub test: usize,
	pub target: (usize, usize),
	pub inspections: RefCell<usize>,
}

impl<'a> Monkey {
	pub fn inspect<const PART: usize>(&self, ppcm: &usize, monkeys: &[Monkey]) {
		*self.inspections.borrow_mut() += self.items.borrow().len();
		self.items.take().into_iter().for_each(|i| {
			let worry_lvl = if PART == 1 {
				(self.operation)(i) / 3
			} else {
				(self.operation)(i) % ppcm
			};

			monkeys[if worry_lvl % self.test == 0 {
				self.target.0
			} else {
				self.target.1
			}]
			.items
			.borrow_mut()
			.push(worry_lvl);
		});
	}

	pub fn new<C: IntoIterator<Item = &'a str>>(c: C) -> Self {
		let mut lines = c.into_iter();

		let items = RefCell::new(
			lines.nth(1).unwrap()[18..]
				.split(", ")
				.map(|i| i.parse::<usize>().unwrap())
				.collect::<Vec<_>>(),
		);

		let operation: Box<dyn Fn(usize) -> usize> = match &lines.next().unwrap()[23..] {
			"* old" => Box::new(|n| n * n),
			op => {
				let mut split = op.split_ascii_whitespace();
				let (operator, operand) = (
					split.next().unwrap(),
					split.next().unwrap().parse::<usize>().unwrap(),
				);

				match operator {
					"+" => Box::new(move |n| n + operand),
					"*" => Box::new(move |n| n * operand),
					_ => panic!("Unknown operation"),
				}
			}
		};

		let test = lines.next().unwrap()[21..].parse::<usize>().unwrap();

		let target = (
			lines.next().unwrap()[29..].parse::<usize>().unwrap(),
			lines.next().unwrap()[30..].parse::<usize>().unwrap(),
		);

		Monkey {
			items,
			operation,
			test,
			target,
			inspections: RefCell::new(usize::MIN),
		}
	}
}
