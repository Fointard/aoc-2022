mod monkey;

use std::collections::BTreeSet;

use itertools::Itertools;
use monkey::Monkey;

const INPUT: &str = include_str!("input.txt");

fn main() {
	solve_part_1();
	solve_part_2();
}

fn compute_monkey_business(monkeys: Vec<Monkey>) -> usize {
	monkeys
		.iter()
		.map(|m| *m.inspections.borrow())
		.collect::<BTreeSet<_>>()
		.iter()
		.rev()
		.take(2)
		.product()
}

fn parse_monkeys() -> (Vec<Monkey>, usize) {
	let mut monkeys = Vec::<Monkey>::new();
	let mut ppcm = 1;

	INPUT.lines().chunks(7).into_iter().for_each(|c| {
		let monkey = Monkey::new(c);
		if ppcm % monkey.test != 0 {
			ppcm *= monkey.test;
		}
		monkeys.push(monkey);
	});

	(monkeys, ppcm)
}

fn solve<const PART: usize, const ROUNDS: usize>() -> usize {
	let (monkeys, ppcm) = parse_monkeys();

	(0..ROUNDS).for_each(|_| {
		monkeys.iter().for_each(|m| {
			m.inspect::<PART>(&ppcm, &monkeys);
		});
	});

	compute_monkey_business(monkeys)
}

fn solve_part_1() -> usize {
	let answer = solve::<1, 20>();
	println!("part 1 answer: {}", answer);
	answer
}

fn solve_part_2() -> usize {
	let answer = solve::<2, 10000>();
	println!("part 2 answer: {}", answer);
	answer
}

#[test]
fn part_1() {
	assert_eq!(solve_part_1(), 99852usize);
}

#[test]
fn part_2() {
	assert_eq!(solve_part_2(), 25935263541usize);
}
