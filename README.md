# [Advent of Code 2022](https://adventofcode.com/2022)

## Two challenges a day for 25 days.

In each folder, run with `cargo r` if you care about the results or `cargo t` if you don't.

