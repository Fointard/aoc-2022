mod cave;

use cave::Cave;

const INPUT: &str = include_str!("input.txt");

fn main() {
	solve_part_1();
	solve_part_2();
}

fn solve_part_1() -> u32 {
	let mut cave = Cave::new::<false>(INPUT);
	let answer = cave.fill::<false>();

	println!("part 1 answer: {}", answer);
	answer
}

fn solve_part_2() -> u32 {
	let mut cave = Cave::new::<true>(INPUT);
	let answer = cave.fill::<true>();

	println!("part 2 answer: {}", answer);
	answer
}

#[test]
fn part_1() {
	assert_eq!(solve_part_1(), 644u32);
}

#[test]
fn part_2() {
	assert_eq!(solve_part_2(), 27324u32);
}
