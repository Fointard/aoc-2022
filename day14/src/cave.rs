use std::{cmp, collections::HashSet};

#[derive(Debug, Default)]
pub struct Cave {
	lowest: u32,
	rocks: HashSet<[u32; 2]>,
	sand: u32,
}

impl Cave {
	fn add_sand<const FLOOR: bool>(&mut self) -> Result<[u32; 2], ()> {
		let mut grain = [500, 0];

		while grain[1] < self.lowest {
			if FLOOR && grain[1] == self.lowest - 1 {
				return Ok(grain);
			} else if !self.rocks.contains(&[grain[0], grain[1] + 1]) {
				grain = [grain[0], grain[1] + 1];
			} else if !self.rocks.contains(&[grain[0] - 1, grain[1] + 1]) {
				grain = [grain[0] - 1, grain[1] + 1];
			} else if !self.rocks.contains(&[grain[0] + 1, grain[1] + 1]) {
				grain = [grain[0] + 1, grain[1] + 1];
			} else if !self.rocks.contains(&grain) {
				return Ok(grain);
			} else {
				break;
			}
		}

		return Err(());
	}

	pub fn fill<const FLOOR: bool>(&mut self) -> u32 {
		while let Ok(coords) = self.add_sand::<FLOOR>() {
			self.rocks.insert(coords);
			self.sand += 1
		}
		self.sand
	}

	pub fn new<const FLOOR: bool>(input: &str) -> Self {
		let mut cave = Cave::default();

		input.lines().for_each(|l| {
			let mut pairs = l.split(" -> ").into_iter();

			let mut pair = parse_pair(pairs.next().unwrap());
			cave.rocks.insert(pair);

			while let Some(next_pair) = pairs.next() {
				let next_pair = parse_pair(next_pair);
				cave.lowest = cmp::max(cave.lowest, next_pair[1]);

				let index = if next_pair[0] == pair[0] { 1 } else { 0 };

				let range = if pair[index] < next_pair[index] {
					pair[index] + 1..=next_pair[index]
				} else {
					next_pair[index]..=pair[index] - 1
				};

				range.for_each(|r| {
					cave.rocks.insert(match index {
						0 => [r, pair[1]],
						1 => [pair[0], r],
						_ => panic!(),
					});
				});

				pair = next_pair;
			}
		});

		cave.lowest += if FLOOR { 2 } else { 0 };

		cave
	}
}

fn parse_pair(pair: &str) -> [u32; 2] {
	let sep = pair.find(",").unwrap();
	[
		pair[..sep].parse::<u32>().unwrap(),
		pair[sep + 1..].parse::<u32>().unwrap(),
	]
}
