use std::collections::HashSet;

const INPUT: &str = include_str!("input.txt");

fn main() {
	solve_part_1();
	solve_part_2();
}

fn solve<const ROPE_SIZE: usize>() -> usize {
	let mut nodes = [(0isize, 0isize); ROPE_SIZE];
	let mut positions = HashSet::<(isize, isize)>::new();

	INPUT.lines().for_each(|l| {
		let mut split = l.split(' ');
		let (direction, moves) = (
			split.next().unwrap(),
			split.next().unwrap().parse::<usize>().unwrap(),
		);

		(0..moves).for_each(|_| {
			match direction {
				"U" => nodes[0].1 += 1,
				"R" => nodes[0].0 += 1,
				"D" => nodes[0].1 -= 1,
				"L" => nodes[0].0 -= 1,
				_ => panic!("Unknown move"),
			}

			(0..ROPE_SIZE - 1).for_each(|n| {
				let (diff_x, diff_y) = (nodes[n].0 - nodes[n + 1].0, nodes[n].1 - nodes[n + 1].1);

				if diff_x.abs() == 2 && diff_y.abs() == 2 {
					nodes[n + 1].0 += diff_x / 2;
					nodes[n + 1].1 += diff_y / 2;
				} else if diff_x.abs() == 2 {
					nodes[n + 1].0 += diff_x / 2;
					nodes[n + 1].1 = nodes[n].1;
				} else if diff_y.abs() == 2 {
					nodes[n + 1].1 += diff_y / 2;
					nodes[n + 1].0 = nodes[n].0;
				}
			});

			positions.insert((nodes[ROPE_SIZE - 1].0, nodes[ROPE_SIZE - 1].1));
		})
	});

	positions.len()
}

fn solve_part_1() -> usize {
	let answer = solve::<2>();
	println!("part 1 answer: {}", answer);
	answer
}

fn solve_part_2() -> usize {
	let answer = solve::<10>();
	println!("part 2 answer: {}", answer);
	answer
}

#[test]
fn part_1() {
	assert_eq!(solve_part_1(), 6026usize);
}

#[test]
fn part_2() {
	assert_eq!(solve_part_2(), 2273usize);
}
