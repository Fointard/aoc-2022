const INPUT: &str = include_str!("input.txt");

fn main() {
	solve_part_1();
	solve_part_2();
}

fn solve_part_1() -> u32 {
	let answer = 0;
	println!("part 1 answer: {}", answer);
	answer
}

fn solve_part_2() -> u32 {
	let answer = 0;
	println!("part 2 answer: {}", answer);
	answer
}

#[test]
fn part_1() {
	assert_eq!(solve_part_1(), 0u32);
}

#[test]
fn part_2() {
	assert_eq!(solve_part_2(), 0u32);
}
