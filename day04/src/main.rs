const INPUT: &str = include_str!("input.txt");

fn main() {
	solve_part_1();
	solve_part_2();
}

fn get_vec_from_line(line: &str) -> Vec<u32> {
	line.split(|c: char| !c.is_ascii_digit())
		.map(|c| c.parse::<u32>().unwrap())
		.collect::<Vec<_>>()
}

fn solve<F: FnMut(Vec<u32>) -> bool>(mut cond: F) -> u32 {
	INPUT
		.lines()
		.fold(0, |acc, l| acc + u32::from(cond(get_vec_from_line(l))))
}

fn solve_part_1() -> u32 {
	let answer = solve(|v| v[0] <= v[2] && v[1] >= v[3] || v[0] >= v[2] && v[1] <= v[3]);
	println!("part 1 answer: {}", answer);
	answer
}

fn solve_part_2() -> u32 {
	let answer = solve(|v| {
		(v[0]..=v[1]).contains(&v[2])
			|| (v[0]..=v[1]).contains(&v[3])
			|| (v[2]..=v[3]).contains(&v[0])
			|| (v[2]..=v[3]).contains(&v[1])
	});
	println!("part 2 answer: {}", answer);
	answer
}

#[test]
fn part_1() {
	assert_eq!(solve_part_1(), 441u32);
}

#[test]
fn part_2() {
	assert_eq!(solve_part_2(), 861u32);
}
